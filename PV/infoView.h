#import <UIKit/UIKit.h>

@interface infoView : UIViewController

@property (nonatomic, strong) UIScrollView *scroll;

@property (nonatomic) UIImageView *pics;
@property (nonatomic) UILabel *name;
@property (nonatomic) UILabel *pop;

@property (nonatomic) NSInteger selected;
@property (nonatomic) NSDictionary *fullDict;
@property (nonatomic) NSMutableArray *fullName;
@property (nonatomic) NSMutableArray *pup;
@property (nonatomic, retain) UITextView *dis;

@property (nonatomic, assign) NSString *cellKeyID;
@end
