#import "infoView.h"


@import UIKit;


@interface infoView()

@property (nonatomic, strong) NSDictionary *imgKeys;
@property (nonatomic, strong) NSDictionary *popKeys;

@property (nonatomic) NSDictionary *keys;

@end





@implementation infoView

@synthesize selected;
@synthesize fullDict;
@synthesize fullName;
@synthesize name;
@synthesize pop;
@synthesize pics;
@synthesize scroll;
@synthesize dis;
@synthesize imgKeys;
@synthesize popKeys;
@synthesize keys;
@synthesize cellKeyID;

#pragma mark loading the view

- (void)loadView
{

	[super loadView];
	[self loadDict];
	[self loadScroll];
	[self addImages];
	[self addLabels];
	
	self.view.backgroundColor = [UIColor whiteColor];
	
	
	//name.text = fullName[selected];
	//pop.text = pup[selected];
	//pop.textAlignment = 1;
	
	
	
}



- (void)viewDidLoad 
{
	[super viewDidLoad];
	
}
	#pragma mark -
	#pragma mark loading the dictionaries


- (void)loadDict 
{
	NSString *mainPath = [[NSBundle mainBundle] pathForResource:@"main" ofType:@"plist"];
	NSString *keyPath = [[NSBundle mainBundle] pathForResource:@"keys" ofType:@"plist"];
	
	
	fullDict = [NSDictionary dictionaryWithContentsOfFile:mainPath];
	
	fullName = [NSMutableArray arrayWithArray:[fullDict objectForKey:@"names"]];
	
	
	
	
	keys = [NSDictionary dictionaryWithContentsOfFile:keyPath];
	
	imgKeys = [NSDictionary dictionaryWithDictionary:[keys objectForKey:@"imgDict"]];
	
	popKeys = [NSDictionary dictionaryWithDictionary:[keys objectForKey:@"popDict"]];
	
	
	
	
}

	#pragma mark -
	#pragma mark ScrollView


- (void) loadScroll
{
	float scrHeight = [UIScreen mainScreen].bounds.size.height;
	float scrWidth = [UIScreen mainScreen].bounds.size.width;
	
	scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, scrWidth, scrHeight)];

	scroll.showsVerticalScrollIndicator = NO;
	scroll.backgroundColor = [UIColor whiteColor];
	//scroll.contentSize = CGSizeMake(scrWidth, 800);
	[self.view addSubview:scroll];
} 

	#pragma mark -
	#pragma mark adding content
	
- (void)addImages 
{
	//float scrHeight = [UIScreen mainScreen].bounds.size.height;
	float scrWidth = [UIScreen mainScreen].bounds.size.width;
	
	pics = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, scrWidth - 10, 200)];
	pics.image = [UIImage imageNamed:[imgKeys objectForKey:cellKeyID]];
	pics.clipsToBounds = YES;
	pics.layer.cornerRadius = 10;
	
	
	[scroll addSubview:pics];
}

- (void)addLabels 
{
//	float scrHeight = [UIScreen mainScreen].bounds.size.height;
	float scrWidth = [UIScreen mainScreen].bounds.size.width;
	
	name = [[UILabel alloc] initWithFrame:CGRectMake(0, pics.frame.size.height + 30, scrWidth, 50)];
	name.text = [NSString stringWithFormat:@"%@- %@", fullName[selected], cellKeyID];
	name.textAlignment = 1;
	[scroll addSubview:name];
	
	
	
	pop = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMidY(name.frame) + 30, scrWidth, 50)];
	
	pop.text = [NSString stringWithFormat:@"Population : %@", [popKeys objectForKey:cellKeyID]];
	pop.textAlignment = 1;
	[scroll addSubview:pop];
	
	
}

@end



