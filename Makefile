ARCHS = arm64 


include $(THEOS)/makefiles/common.mk

APPLICATION_NAME = statelist

$(APPLICATION_NAME)_FILES = $(wildcard *.m) $(wildcard FW/*.m) $(wildcard PV/*.m)

$(APPLICATION_NAME)_CFLAGS = -fobjc-arc

$(APPLICATION_NAME)_FRAMEWORKS = UIKit CoreGraphics QuartzCore

include $(THEOS_MAKE_PATH)/application.mk

after-install::
	install.exec "killall \"statelist\"" || true
