#import "MainVC.h"
#import "FW/iCarousel.h"
#import "FW/Colours.h"
#import "RootViewController.h"



@interface MainVC() <iCarouselDataSource, iCarouselDelegate>

@property (nonatomic, strong) iCarousel *acarousel;
@property (nonatomic, strong) NSMutableArray *cons;
@end



@implementation MainVC

@synthesize acarousel;
@synthesize cons;



- (void)dealloc
{   
    acarousel.delegate = nil;
    acarousel.dataSource = nil;
}

#pragma mark -
#pragma mark View lifecycle

- (void)loadView {
	
	[super loadView];
	
	NSString *mainPath = [[NSBundle mainBundle] pathForResource:@"main" ofType:@"plist"];
	
	
	NSDictionary *main = [NSDictionary dictionaryWithContentsOfFile:mainPath];
	
	
	cons = [NSMutableArray arrayWithArray:[main objectForKey:@"countries"]];
	
	
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.title = @"Conogama";
	self.view.backgroundColor = [UIColor whiteColor];
	
	#pragma mark carousel
	
	//create carousel
	acarousel = [[iCarousel alloc] initWithFrame:self.view.bounds];
	acarousel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	acarousel.backgroundColor = [UIColor whiteColor];//colorWithRed:0.96 green:0.89 blue:0.73 alpha:1.0];
    acarousel.type = iCarouselTypeCoverFlow;
    acarousel.centerItemWhenSelected = YES;
	acarousel.delegate = self;
	acarousel.dataSource = self;

	//add carousel to view
	[self.view addSubview:acarousel];
	
	
	
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.acarousel = nil;
}



#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
	int cnt;
	cnt = cons.count;    
    return cnt;
}



- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
UILabel *label = nil;
UIButton *mainBtn = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        //don't do anything specific to the index within
        //this `if (view == nil) {...}` statement because the view will be
        //recycled and used with other index values later
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 240.0f, 240.0f)];
       //((UIImageView *)view).image = [UIImage imageNamed:@"background.jpg"];
        view.contentMode = UIViewContentModeCenter;
        view.backgroundColor = [UIColor blackColor];
        
        view.clipsToBounds = YES;

		view.layer.cornerRadius = 10;
		view.userInteractionEnabled = YES;
		
		
		
		label = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, view.frame.size.width, view.frame.size.height)];
		
		label.font = [UIFont systemFontOfSize:60];
		label.textColor = [UIColor seafoamColor];
		label.tag = 1;
		label.textAlignment = NSTextAlignmentCenter;
		
		[view addSubview:label];
		
		
		
		mainBtn = [UIButton buttonWithType:UIButtonTypeCustom];
		mainBtn.frame = CGRectMake(0.0f, 0.0f, view.frame.size.width, view.frame.size.height);
		mainBtn.backgroundColor = [UIColor clearColor];
		[mainBtn addTarget:self action:@selector(mainTap:) forControlEvents:UIControlEventTouchUpInside];
		[mainBtn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
		mainBtn.tag = 5;
		[view addSubview:mainBtn];
       
       
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
        mainBtn = (UIButton *)[view viewWithTag:5];
    }
       
       
       
       
    label.text = cons[index];
       
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
    	case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return NO;
        }
        
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.2f;
        }
        
        default:
        {
            return value;
        }
    }
}

#pragma mark -
#pragma mark button

- (void)mainTap:(UIButton *)sender
{
	//get item index for button
	
	//NSInteger index = [acarousel indexOfItemViewOrSubview:sender];
	
	RootViewController *root = [[RootViewController alloc] init];
	[self.navigationController pushViewController:root animated:YES];
	
	
	
}


@end