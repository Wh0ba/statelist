#import "RootViewController.h"
#import "PV/infoView.h"

#import "PV/cello.h"


@interface RootViewController()
@property (nonatomic) NSMutableArray *names;
@property (nonatomic) NSMutableArray *keys;
@end


@implementation RootViewController

@synthesize names;
@synthesize keys;

- (void)loadDict {
	
	
	
	NSString *mainPath = [[NSBundle mainBundle] pathForResource:@"main" ofType:@"plist"];
	
	
	NSDictionary *main = [NSDictionary dictionaryWithContentsOfFile:mainPath];
	
	
	keys = [NSMutableArray arrayWithArray:[main objectForKey:@"keys"]];
	
	names = [NSMutableArray arrayWithArray:[main objectForKey:@"names"]];
	
}


- (void)loadView {
	[super loadView];
	[self loadDict];

	self.title = @"States";
	
	
	
	
	
	
	//self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addButtonTapped:)];
	
}




/*
-(void)submit{


[names insertObject:cellId atIndex:0];
	[self.tableView insertRowsAtIndexPaths:@[ [NSIndexPath indexPathForRow:0 inSection:0] ] withRowAnimation:UITableViewRowAnimationAutomatic];


}
*/


- (void)addButtonTapped:(id)sender {
	
	
	
}

#pragma mark -
#pragma mark section and row count

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	if (section == 0) {
		return names.count;
	}else {
		return 5;
	}
	
}
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
     return 20.0;
}
 
-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
     return 8.0;
}
 
 
-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section
{
     return [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark -
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

	UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 20)];
	
	[title setBackgroundColor:[UIColor blackColor]];
	[title setTextAlignment:1];
	title.textColor = [UIColor greenColor];
	[title setFont:[UIFont boldSystemFontOfSize:15]];
	
	
	if (section == 0){
		[title setText:@"Offical States"];
		return title ;
    }else {
        [title setText:@"SubStates"];
        return title ; 
    }
    
}



#pragma mark -
#pragma mark creating cells 

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

	cello *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];

	if (!cell) {
		cell = [[cello alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
	}

	//cell.textLabel.text = names[indexPath.row];
	NSString *name = names[indexPath.row];
	
	NSString *key = keys[indexPath.row];
	if (indexPath.section == 0) {
		cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@",key,name];
	cell.keyID = keys[indexPath.row];
	}else if (indexPath.section == 1){
		
		cell.textLabel.text = @"IDK - NOT YET";
		cell.keyID = @"IDK";
		
	}
	
	
	
	return cell;
}






#pragma mark -
#pragma mark selecting cells

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	cello *cell = [tableView cellForRowAtIndexPath:indexPath];
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	//names[indexPath.row]
	
	if (indexPath.section == 0) {
		infoView *infoV = [[infoView alloc] init];
	
	
	infoV.selected = indexPath.row;
	infoV.cellKeyID = cell.keyID;

	[self.navigationController pushViewController:infoV animated:YES];
	}
	
	
	
	
	
	
	
}



@end
